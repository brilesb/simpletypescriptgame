﻿module TypeScriptApp {
    export class Ship extends Phaser.Sprite {
        shipSprite: Phaser.Sprite;
        exhaustEmitter: Phaser.Particles.Arcade.Emitter;
        shipType: ShipType;
        nextFire: number;
        fireIndex: number;
        accelVector: Phaser.Point; // re-using the same object every update frame
        lastFiringPoint: Phaser.Point;
        aiController: AIController; // this should be a generic controller interface instead


        // stat variables (health, speed, etc...)

        constructor(game: Phaser.Game, x: number, y: number, shipType: ShipType) {
            super(game, x, y);
            game.add.existing(this);
            this.anchor.set(0.5, 0.5);

            this.shipType = shipType;
            this.maxHealth = shipType.maxHealth;
            this.health = this.maxHealth;
            this.nextFire = 0;
            this.fireIndex = 0;
            this.accelVector = new Phaser.Point(0, 0);
            
            this.game.physics.enable(this, Phaser.Physics.ARCADE);
            this.shipSprite = this.game.add.sprite(0, 0, "space", this.shipType.spriteKey);
            this.shipSprite.anchor.set(0.5, 0.5);

            if (this.shipType.rotationSpeed != 0)
                this.body.angularVelocity = this.shipType.rotationSpeed;
            
            //this.body.setSize(this.shipSprite.width, this.shipSprite.height, -this.shipSprite.width / 2, -this.shipSprite.height / 2);
            this.body.collideWorldBounds = true;
            this.body.bounce.set(0.5);
            
            this.setupExhaustEmitter();
            this.addChild(this.shipSprite);
        }

        showExhaust(exhaustOn: boolean) {
            if (this.shipType.exhaustType == ExhaustType.SINGLE) {
                this.exhaustEmitter.on = exhaustOn;
                if (this.shipType.boostSound) {
                    if (exhaustOn && !this.shipType.boostSound.isPlaying)
                        this.shipType.boostSound.play(null, 0, 0.5, true);
                    else if (!exhaustOn && this.shipType.boostSound.isPlaying)
                        this.shipType.boostSound.stop();
                }
            }
        }

        setupExhaustEmitter() {
            if (this.shipType.exhaustType == ExhaustType.SINGLE) {
                this.exhaustEmitter = this.game.add.emitter(0, 0, 50);
                this.exhaustEmitter.makeParticles("effects",['fire11', 'fire12', 'fire13','fire14','fire15'],
                    200, false, false);
                this.addChild(this.exhaustEmitter);

                this.exhaustEmitter.gravity = 200;
                this.exhaustEmitter.setAlpha(0.5, 0, 500);
                this.exhaustEmitter.minParticleSpeed.set(0, -1);
                this.exhaustEmitter.maxParticleSpeed.set(0, -1);
                this.exhaustEmitter.x = 0;
                this.exhaustEmitter.y = this.height / 2 + 10;
                this.exhaustEmitter.setRotation(0, 0);
                this.exhaustEmitter.start(false, 500, 100);
            }
        }

        update() {
            
        }

        stop() {
            this.body.angularVelocity = 0;
            //this.body.velocity.x = 0;
            //this.body.velocity.y = 0;
        }

        move(direction: Direction) {
            if (direction == Direction.UP) {
                this.game.physics.arcade.velocityFromAngle(this.angle - 90, this.shipType.acceleration, this.accelVector);
            }
            else if (direction == Direction.DOWN) {
                this.game.physics.arcade.velocityFromAngle(this.angle + 90, this.shipType.acceleration, this.accelVector);
            }
            if (!this.accelVector.isZero()) {
                this.body.velocity = this.body.velocity.add(this.accelVector.x, this.accelVector.y);

                // check max speed
                var magSqr: number = this.body.velocity.getMagnitudeSq();
                if (magSqr > this.shipType.maxSpeedSqr)
                    this.body.velocity.setMagnitude(this.shipType.maxSpeed);
                this.accelVector.setTo(0, 0);
                
            }                    
        }
 
        rotate(direction: Direction) {
            
            if (direction == Direction.LEFT) {
                this.body.angularVelocity = -200;
            }
            else if (direction == Direction.RIGHT) {
                this.body.angularVelocity = 200;
            }
            else {
                // this is not a 3D game
            }
        }

        fire() {
            if (this.game.time.now > this.nextFire) {
                this.nextFire = this.game.time.now + this.shipType.fireRate;

                var bullet: Bullet = this.shipType.bulletGroup.getFirstDead();

                if (bullet) {
                    var firePoint: Phaser.Point = this.getFirePoint();
                    this.lastFiringPoint = firePoint;
                    bullet.fire(this.angle - 90, firePoint.x, firePoint.y, this.body.velocity);
                    this.shipType.fireSound.play();
                }
            }
        }

        private getFirePoint(): Phaser.Point {
            var Ox, Oy, Rx, Ry, theta: number;
            var fireOffset: Phaser.Point = this.shipType.firePoints[this.fireIndex];
            Ox = fireOffset.x;
            Oy = fireOffset.y;

            this.fireIndex = (this.fireIndex + 1) % this.shipType.firePoints.length;
            
            theta = (Math.PI / 180) * ((this.angle - 90 + 360) % 360); //convert to radians

            //The rotated position of this point in world coordinates    
            Rx = this.x + (Ox * Math.cos(theta)) - (Oy * Math.sin(theta));
            Ry = this.y + (Ox * Math.sin(theta)) + (Oy * Math.cos(theta));

            return new Phaser.Point(Rx, Ry);
        }

        kill(): Phaser.Sprite {
            var explosionAnimation:Phaser.Sprite = this.shipType.explosionPool.getFirstExists(false);
            explosionAnimation.reset(this.x, this.y);
            explosionAnimation.play('explode', 30, false, true);
            this.shipType.explodeSound.play();
            return super.kill();
        }

        hit(ship, bullet) {
            bullet.kill();
            this.health -= bullet.bulletType.damage;
            if (this.health <= 0) {
                this.kill();
            }
        }

    }
}