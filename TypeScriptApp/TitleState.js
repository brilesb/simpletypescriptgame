var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var TypeScriptApp;
(function (TypeScriptApp) {
    var TitleState = (function (_super) {
        __extends(TitleState, _super);
        function TitleState() {
            _super.apply(this, arguments);
        }
        TitleState.prototype.preload = function () {
        };
        TitleState.prototype.create = function () {
            alert("inside the title state");
            var text = "Hello World!";
            var style = { font: "65px Arial", fill: "#ff0000", align: "center" };
            this.game.add.text(0, 0, text, style);
            this.input.onTap.addOnce(this.titleClicked, this);
        };
        TitleState.prototype.titleClicked = function () {
            //this.game.state.start("Game");
        };
        return TitleState;
    })(Phaser.State);
    TypeScriptApp.TitleState = TitleState;
})(TypeScriptApp || (TypeScriptApp = {}));
//# sourceMappingURL=TitleState.js.map