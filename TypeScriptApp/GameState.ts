﻿module TypeScriptApp {
    export class GameState extends Phaser.State {
        player1: Ship;
        p1Controller: PlayerShipController;
        bulletTypes: Array<BulletType>;
        shipTypes: Array<ShipType>;
        playerBullets: BulletGroup;
        enemyBullets: BulletGroup;
        enemies: EnemyGroup;
        obstacles: Phaser.Group;
        explosions: Phaser.Group;
        music: Phaser.Sound;
        foreground: Phaser.Group;
        midground: Phaser.Group;
        background: Phaser.Group;
        livingEnemies: number;
        isGameOver: boolean = false;

        create() {
            this.game.world.setBounds(-1000, -1000, 2000, 2000);

            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            this.game.physics.arcade.gravity.y = 0;

            this.game.add.tileSprite(-1000, -1000, 2000, 2000, "starfield");

            this.background = this.game.add.group();
            this.midground = this.game.add.group();
            this.foreground = this.game.add.group();

            this.setupBulletGroups();
            this.createExplosionPools();
            this.setupShipTypes();

            this.player1 = new Ship(this.game, 0,
                0, this.shipTypes[0]);
            this.midground.add(this.player1);
            this.game.camera.follow(this.player1);

            this.p1Controller = new PlayerShipController(this.game, this.player1);

            this.createBackgroundShips();
            this.createObsatcles();
            this.createEnemyWaves();
            

            // start the background music
            this.music = this.game.add.audio("music", 0.25);
            this.music.loop = true;
            this.music.play(); // disabled so I can work in silence

            this.setupUI();
            this.livingEnemies = 0;
        }

        setupBulletGroups() {
            // I would probably load these settings from json or something
            this.bulletTypes = new Array<BulletType>();
            this.bulletTypes.push(new BulletType("laserGreen", 600, 10));
            this.bulletTypes.push(new BulletType("laserRed", 600, 10));
            this.bulletTypes.push(new BulletType("laserBlue", 600, 10));

            this.playerBullets = new BulletGroup(this.game, this.bulletTypes[0], 50);
            this.game.add.existing(this.playerBullets);
            this.midground.add(this.playerBullets);

            this.enemyBullets = new BulletGroup(this.game, this.bulletTypes[1], 50);
            this.game.add.existing(this.enemyBullets);
            this.midground.add(this.enemyBullets);
        }

        createExplosionPools() {
            this.explosions = this.game.add.group();

            for (var i: number = 0; i < 20; i++) {

                var explosionAnimation: Phaser.Sprite = this.explosions.create(0, 0, 'effects', 0, false);
                explosionAnimation.anchor.setTo(0.5, 0.5);
                explosionAnimation.animations.add("explode", Phaser.Animation.generateFrameNames('spaceEffects_', 8, 16, '', 3));
            }
        }

        setupShipTypes() {
            this.shipTypes = [];
            var firePoints: Array<Phaser.Point> = [new Phaser.Point(10, 25), new Phaser.Point(10, -25)];
            var enemyFirePoints: Array<Phaser.Point> = [new Phaser.Point(10, 0)];
            var fireSound: Phaser.Sound = this.game.add.audio("laser");
            var enemyFireSound: Phaser.Sound = this.game.add.audio("redLaser");
            var explodeSound: Phaser.Sound = this.game.add.audio("explode");
            var boostSound: Phaser.Sound = this.game.add.audio("boost");
            this.shipTypes.push(new ShipType("playerShip1_blue", 100, 300, 10, 250, this.playerBullets, firePoints, fireSound, this.explosions, explodeSound, boostSound));
            this.shipTypes.push(new ShipType("spaceStation_024", 0, 0, 0, 0, null, null, null, this.explosions, explodeSound, null, 5, ExhaustType.NONE));
            this.shipTypes.push(new ShipType("spaceStation_018", 0, 0, 0, 0, null, null, null, this.explosions, explodeSound, null, -25, ExhaustType.NONE));
            this.shipTypes.push(new ShipType("spaceStation_019", 0, 0, 0, 0, null, null, null, this.explosions, explodeSound, null, 40, ExhaustType.NONE));
            this.shipTypes.push(new ShipType("spaceMeteors_001", 0, 0, 0, 0, null, null, null, this.explosions, explodeSound, null, -50, ExhaustType.NONE));
            this.shipTypes.push(new ShipType("meteorBrown_big1", 0, 0, 0, 0, null, null, null, this.explosions, explodeSound, null, 25, ExhaustType.NONE));
            this.shipTypes.push(new ShipType("meteorBrown_big2", 0, 0, 0, 0, null, null, null, this.explosions, explodeSound, null, -60, ExhaustType.NONE));
            this.shipTypes.push(new ShipType("meteorBrown_med1", 0, 0, 0, 0, null, null, null, this.explosions, explodeSound, null, 75, ExhaustType.NONE));
            this.shipTypes.push(new ShipType("spaceShips_002", 10, 150, 10, 400, this.enemyBullets, enemyFirePoints, enemyFireSound, this.explosions, explodeSound, null));
            
        }

        createBackgroundShips() {
            this.background.add(new Ship(this.game, 150, 175, this.shipTypes[1]));
            
        }

        createObsatcles() {
            this.obstacles = this.game.add.group();
            this.midground.add(this.obstacles);
            this.obstacles.add(new Ship(this.game, -400, 600, this.shipTypes[5]));
            this.obstacles.add(new Ship(this.game, -700, 420, this.shipTypes[6]));
            this.obstacles.add(new Ship(this.game, -920, 875, this.shipTypes[5]));
            this.obstacles.add(new Ship(this.game, 200, -325, this.shipTypes[6]));
            this.obstacles.add(new Ship(this.game, 300, -675, this.shipTypes[5]));
            this.obstacles.add(new Ship(this.game, 600, -105, this.shipTypes[7]));
            this.obstacles.add(new Ship(this.game, 701, 653, this.shipTypes[7]));

            this.obstacles.add(new Ship(this.game, 800, 750, this.shipTypes[2]));
            this.obstacles.add(new Ship(this.game, -700, -500, this.shipTypes[3]));
        }

        createEnemyWaves() {
            this.enemies = new EnemyGroup(this.game);

            /*var enemy: Ship = this.enemies.createEnemy(this.shipTypes[8]);
            enemy.aiController = new AIController(enemy, this.player1);
            this.enemies.addToWave(0, enemy);

            //for (var i: number = 0; i < 3; i++) {
                
            //}

            this.enemies.startNextWave();*/
        }

        setupUI() {
            
        }

        gameOver() {
            var style = { font: "64px Arial", fill: "#ffffff", align: "center" };
            this.game.add.text(this.player1.x - 140, this.player1.y - 35, "Game Over", style);
            this.isGameOver = true;
            this.player1.shipType.boostSound.stop(); // stop boost sound if boosting during death
        }

        update() {

            this.game.input.update();
            if (this.player1.alive) {
                this.p1Controller.update();

                this.game.physics.arcade.collide(this.player1, this.obstacles);
                this.game.physics.arcade.collide(this.enemies);
                this.game.physics.arcade.collide(this.enemies, this.obstacles);
                this.game.physics.arcade.collide(this.obstacles);
                this.game.physics.arcade.overlap(this.enemyBullets, this.player1, this.player1.hit, null, this.player1);

                this.livingEnemies = 0;
                this.enemies.forEachAlive(function (enemy: Ship) {
                    this.game.physics.arcade.collide(this.player1, enemy);
                    this.game.physics.arcade.overlap(this.playerBullets, enemy, enemy.hit, null, enemy);
                    enemy.update();
                    if (enemy.aiController)
                        enemy.aiController.update();
                    this.livingEnemies++;
                }, this);
            }
            else if (!this.isGameOver) {
                this.gameOver();
            }

            // check for wave death
            if (this.livingEnemies == 0) {
                this.enemies.generateBasicWave(this.shipTypes[8], this.player1);
                this.enemies.startNextWave();
            }
        }

        render() {
            this.game.debug.text(this.game.time.fps.toString() || '--', 2, 14, "#00ff00");

            //this.game.debug.body(this.player1);
            //this.game.debug.spriteInfo(this.testEnemy, 32, 32);


            this.game.debug.text("health: " + this.player1.health, 5, 30);
            this.game.debug.text("wave: " + this.enemies.nextWave, 630, 30);
            this.game.debug.text("# enemies: " + this.livingEnemies, 630, 50);
            //if (this.player1.body.velocity)
            //    this.game.debug.text("velocity: (" + this.player1.body.velocity.x.toString() + ", " + this.player1.body.velocity.y.toString() + ")", 400, 15);
        }
    }
}