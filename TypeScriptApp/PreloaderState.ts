﻿module TypeScriptApp {
    export class PreloaderState extends Phaser.State {
    
        preload() {
            // setup preload bar

            this.load.image("starfield", "assets/sprites/starfield.png");
            this.load.image("starfieldBlue", "assets/sprites/starfield_blue.png");
            this.load.image("logo", "assets/sprites/space_logo.png");
            this.load.atlasJSONArray("space", "assets/sprites/spaceatlas.png", "assets/sprites/spaceatlas.json");
            this.load.atlasJSONArray("effects", "assets/sprites/effects.png", "assets/sprites/effects.json");
            
            this.load.audio("music", "assets/music/Cyborg Ninja.mp3");
            this.load.audio("laser", "assets/sounds/laser5.mp3");
            this.load.audio("redLaser", "assets/sounds/phaserUp6.mp3");
            this.load.audio("explode", "assets/sounds/explosion.wav");
            this.load.audio("boost", "assets/sounds/boost_loop.wav");
        }

        create() {
            this.startMainMenu();
        }

        startMainMenu() {
            this.game.state.start('MainMenu', true, false);
        }
    }
}