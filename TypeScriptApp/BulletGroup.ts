﻿module TypeScriptApp {
    export class BulletGroup extends Phaser.Group {
        bulletType: BulletType;

        constructor(game: Phaser.Game, bulletType: BulletType, count: number) {
            super(game, game.world, bulletType.spriteKey + "_group", false, true, Phaser.Physics.ARCADE);
            this.bulletType = bulletType;
            this.createBullets(count);
        }

        createBullets(count: number) {
            for (var i: number = 0; i < count; i++) {
                var bullet: Bullet = new Bullet(this.game, 0, 0, this.bulletType);
                bullet.anchor.x = 0.5;
                bullet.anchor.y = 0.5;
                bullet.outOfBoundsKill = true;
                bullet.checkWorldBounds = true;
                bullet.kill();
                this.game.add.existing(bullet);
                this.add(bullet);
            }
        }
    }
}