﻿module TypeScriptApp {
    export class ShipType {
        spriteKey: string;
        maxHealth: number;
        acceleration: number;
        maxSpeed: number;
        maxSpeedSqr: number;
        fireRate: number;
        bulletGroup: BulletGroup;
        rotationSpeed: number;
        rotationSpeedRad: number;
        exhaustType: ExhaustType;
        firePoints: Array<Phaser.Point>;
        fireSound: Phaser.Sound;
        explosionPool: Phaser.Group;
        explodeSound: Phaser.Sound;
        boostSound: Phaser.Sound;

        constructor(key: string, maxHealth: number, maxSpeed: number, acceleration: number, fireRate: number, bulletGroup: BulletGroup, firePoints: Array<Phaser.Point>, fireSound: Phaser.Sound, explosionGroup: Phaser.Group, explodeSound: Phaser.Sound, boostSound: Phaser.Sound, rotationSpeed: number = 0, exhaustType: ExhaustType = ExhaustType.SINGLE) {
            this.spriteKey = key;
            this.maxHealth = maxHealth;
            this.maxSpeed = maxSpeed;
            this.maxSpeedSqr = maxSpeed * maxSpeed;
            this.acceleration = acceleration;
            this.fireRate = fireRate;
            this.bulletGroup = bulletGroup;
            this.fireSound = fireSound;
            this.firePoints = firePoints;
            this.explosionPool = explosionGroup;
            this.explodeSound = explodeSound;
            this.boostSound = boostSound;
            this.rotationSpeed = rotationSpeed;
            this.rotationSpeedRad = (Math.PI / 180) * this.rotationSpeed;
            this.exhaustType = exhaustType;
        }
    }

    export enum ExhaustType {
        NONE,
        SINGLE,
        DOUBLE
    }
}