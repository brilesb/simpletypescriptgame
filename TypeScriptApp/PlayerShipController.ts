﻿module TypeScriptApp {
    export class PlayerShipController {
        ship: Ship;
        keys: Keys

        constructor(game: Phaser.Game, ship: Ship) {
            this.ship = ship;

            this.setupKeys(game);
        }

        setupKeys(game: Phaser.Game) {
            this.keys = new Keys();

            this.keys.up = game.input.keyboard.addKey(Phaser.Keyboard.UP);
            this.keys.down = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
            this.keys.left = game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
            this.keys.right = game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
            this.keys.fire = game.input.keyboard.addKey(Phaser.Keyboard.CONTROL);
        }


        update() {
            var moveKeyPressed: boolean = false;
            this.ship.stop();
            if (this.keys.down.isDown) {
                this.ship.move(Direction.DOWN);
                moveKeyPressed = true;
            }
            else if (this.keys.up.isDown) {
                this.ship.move(Direction.UP);
                moveKeyPressed = true;
            }
            if (this.keys.left.isDown)
                this.ship.rotate(Direction.LEFT);
            if (this.keys.right.isDown)
                this.ship.rotate(Direction.RIGHT);

            if (this.keys.fire.isDown)
                this.ship.fire();

            this.ship.showExhaust(moveKeyPressed);
        }
    }

    export class Keys {
        up: Phaser.Key;
        down: Phaser.Key;
        left: Phaser.Key;
        right: Phaser.Key;
        fire: Phaser.Key;

    }
}