﻿module TypeScriptApp {
    export class Bullet extends Phaser.Sprite {
        bulletType: BulletType;

        constructor(game: Phaser.Game, x, y, type: BulletType) {
            super(game, x, y, "space", type.spriteKey);
            this.bulletType = type;

        }

        fire(angle: number, x: number, y: number, startVelocity: Phaser.Point) {
            this.reset(x, y);
            this.angle = angle - 90;
            this.game.physics.arcade.velocityFromAngle(angle, this.bulletType.speed, this.body.velocity);
            this.body.velocity.add(startVelocity.x, startVelocity.y);
        }
    }
}