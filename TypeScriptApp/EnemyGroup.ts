﻿module TypeScriptApp {
    export class EnemyGroup extends Phaser.Group {
        waves: Array<Array<Ship>>;
        nextWave: number;

        constructor(game: Phaser.Game) {
            super(game);
            this.waves = [];
            this.nextWave = 0;
        }

        createEnemy(shipType: ShipType): Ship {
            var enemy: Ship = new Ship(this.game, -5000, -5000, shipType);
            enemy.alive = false;
            enemy.showExhaust(false);
            this.add(enemy);
            return enemy;
        }

        addToWave(waveNumber: number, enemy: Ship) {
            if (!this.waves[waveNumber])
                this.waves[waveNumber] = [];
            this.waves[waveNumber].push(enemy);
        }

        startNextWave() {
            for (let enemy of this.waves[this.nextWave]) {
                this.placeEnemyInWorld(enemy);
            }
            this.nextWave++;
        }

        generateBasicWave(shipType: ShipType, player: Ship) {
            for (var i: number = 0; i < this.nextWave + 1; i++) {
                
                var enemy: Ship = this.createEnemy(shipType);
                enemy.aiController = new AIController(enemy, player);

                this.addToWave(this.nextWave,enemy);
            }
        }

        protected placeEnemyInWorld(enemy: Ship) {

            var side:number = this.game.rnd.integerInRange(1, 4);
            var x: number = 0;
            var y: number = 0;
            if (side == 1) {// left
                x = -1100;
                y = this.game.rnd.integerInRange(-1000, 1000);
            }
            else if (side == 2) { // right
                x = 1100;
                y = this.game.rnd.integerInRange(-1000, 1000);
            }
            else if (side == 3) { // top
                x = this.game.rnd.integerInRange(-1000, 1000);
                y = -1100;
            }
            else if (side == 4) { // bottom
                x = this.game.rnd.integerInRange(-1000, 1000);
                y = 1100;
            }
            enemy.reset(x, y);
        }
    }
}