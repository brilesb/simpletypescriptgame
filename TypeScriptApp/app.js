var TypeScriptApp;
(function (TypeScriptApp) {
    var SimpleGame = (function () {
        function SimpleGame() {
            this.game = new Phaser.Game(800, 600, Phaser.AUTO, 'content', null);
            this.game.state.add("Boot", TypeScriptApp.BootState, false);
            this.game.state.add("Title", TypeScriptApp.TitleState, false);
            this.game.state.start("Boot");
        }
        return SimpleGame;
    })();
    TypeScriptApp.SimpleGame = SimpleGame;
})(TypeScriptApp || (TypeScriptApp = {}));
window.onload = function () {
    var game = new TypeScriptApp.SimpleGame();
};
//# sourceMappingURL=app.js.map