﻿module TypeScriptApp {
    export class AIController {
        ship: Ship;
        targetShip: Ship;
        fireDistance: number = 250;

        constructor(ship: Ship, targetShip: Ship) {
            this.ship = ship;
            this.targetShip = targetShip;
        }

        update() {
            this.ship.stop();

            if (this.targetShip) {
                var angleToTarget: number = this.ship.game.physics.arcade.angleBetween(this.ship, this.targetShip);

                var exactRotation: number = angleToTarget + Math.PI / 2;
                this.ship.rotation = exactRotation; // ships rotate instantaneously
                // doesn't handle knowing which direction to turn or avoiding bouncing around the correct direction
                //if (exactRotation < this.ship.rotation)
                //    this.ship.rotate(Direction.LEFT);
                //else if (exactRotation > this.ship.rotation)
                //    this.ship.rotate(Direction.RIGHT);

                var targetDistance: number = this.ship.position.distance(this.targetShip.position);
                if (targetDistance <= this.fireDistance)
                    this.ship.fire();
                else
                    this.ship.move(Direction.UP);


            }
        }
    }
}