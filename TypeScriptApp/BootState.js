var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var TypeScriptApp;
(function (TypeScriptApp) {
    var BootState = (function (_super) {
        __extends(BootState, _super);
        function BootState() {
            _super.apply(this, arguments);
        }
        BootState.prototype.preload = function () {
        };
        BootState.prototype.create = function () {
            //  Unless you specifically need to support multitouch I would recommend setting this to 1
            this.input.maxPointers = 1;
            //  Phaser will automatically pause if the browser tab the game is in loses focus. You can disable that here:
            this.stage.disableVisibilityChange = true;
            alert("inside the boot state");
            var text = "Hello World!";
            var style = { font: "65px Arial", fill: "#ff0000", align: "center" };
            this.game.add.text(0, 0, text, style);
        };
        return BootState;
    })(Phaser.State);
    TypeScriptApp.BootState = BootState;
})(TypeScriptApp || (TypeScriptApp = {}));
//# sourceMappingURL=BootState.js.map