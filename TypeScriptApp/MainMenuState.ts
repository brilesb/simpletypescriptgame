﻿module TypeScriptApp {
    export class MainMenuState extends Phaser.State {

        preload() {
        }

        create() {
            this.game.add.tileSprite(0, 0, 800, 600, "starfieldBlue");

            var logo: Phaser.Sprite = this.game.add.sprite(this.game.width / 2, this.game.height / 2 - 100, "logo");
            logo.anchor.set(0.5, 0.5);

            var text:string = "Click to Start";
            var style = { font: "32px Arial", fill: "#ffffff", align: "center" };
            this.game.add.text(this.game.width / 2 - 100, 400, text, style);
            this.input.onTap.addOnce(this.titleClicked, this);
        }

        titleClicked() {
            this.game.state.start("Game");
            
        }
    }

    export enum Direction {
        UP,
        DOWN,
        LEFT,
        RIGHT
    }
}