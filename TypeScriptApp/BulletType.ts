﻿module TypeScriptApp {
    export class BulletType {
        spriteKey: string;
        speed: number;
        damage: number;

        constructor(key: string, speed: number, damage: number) {
            this.spriteKey = key;
            this.speed = speed;
            this.damage = damage;
        }

    }
}