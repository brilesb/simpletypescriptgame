# README #

This is a simple game written in Typescript using the Phaser game engine. Solutions files are from Visual Studio 2015.

The game is a space game where you fly around a small area and shoot enemy spaceships in increasing numbers until you lose.